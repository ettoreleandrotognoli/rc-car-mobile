/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

import React, { Component } from "react";
import { PermissionsAndroid, ScrollView } from "react-native";
import { BleManager, Device, State } from "react-native-ble-plx";
import { Platform, StyleSheet, Text, View, Button, Alert } from "react-native";
import { BluetoothManager, ConnectedBTDevice } from "./app/bluetooth";
import { tap, mergeMap, flatMap } from "rxjs/operators";
import BluetoothSerial from "react-native-bluetooth-serial-next";
import BTDeviceItem from "./app/bluetooth/device/BTDeviceItem";
import { from, merge, of } from "rxjs";
import { asBytes, VehicleState } from "./app/vehicle";
import Joystick from "./app/gamepad/Joystick";
import Driving from "./app/drive/Driving";
import { createStackNavigator, createAppContainer } from 'react-navigation';
import BTDeviceList from "./app/bluetooth/device/BTDeviceList";
import Empty from "./app/layout/Empty";

const MainNavigator = createStackNavigator({
  Devices: {
    screen: Empty,
    params: {
      component: BTDeviceList,
      actions: {
        deviceSelected: (navigation: any, btDevice: ConnectedBTDevice) => {
          navigation.navigate('Drive', {
            properties: {
              listener: (it: VehicleState) => {
                console.log(it);
                btDevice.connection.writeToDevice(asBytes(it));
              }
            }
          });
        }
      },
    },
    navigationOptions: {
      title: "Escolha o dispositivo",
    }
  },
  Drive: {
    screen: Empty,
    params: {
      component: Driving,
    },
    navigationOptions: {
      title: "=)"
    }
  },
});

const App = createAppContainer(MainNavigator);

export default App;