import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Animated,
  PanResponder,
  PanResponderInstance
} from "react-native";

export type Point2D = [number, number];

export interface JoystickValue {
  absolute: Point2D;
  relative: Point2D;
}

interface Props {
  listener: (value: JoystickValue) => void;
}

interface State {
  fingerPosition: Animated.ValueXY;
  valuePosition: Animated.ValueXY;
}

let CIRCLE_RADIUS = 36;
let Window = Dimensions.get("window");

export default class Joystick extends React.Component<Props, State> {
  protected panResponder: PanResponderInstance;
  protected style = StyleSheet.create({
    mainContainer: {
      flex: 1
    },
    limits: {
      width: CIRCLE_RADIUS * 4,
      height: CIRCLE_RADIUS * 4,
      borderRadius: CIRCLE_RADIUS * 2,
      backgroundColor: "#AAAAAA"
    },
    value: {
      zIndex: 1,
      margin: CIRCLE_RADIUS,
      backgroundColor: "#ff0000",
      width: CIRCLE_RADIUS * 2,
      height: CIRCLE_RADIUS * 2,
      borderRadius: CIRCLE_RADIUS
    },
    finger: {
      zIndex: 2,
      margin: CIRCLE_RADIUS,
      marginTop: -CIRCLE_RADIUS * 3,
      backgroundColor: "#0000ff",
      width: CIRCLE_RADIUS * 2,
      height: CIRCLE_RADIUS * 2,
      borderRadius: CIRCLE_RADIUS,
      opacity: 0.1
    }
  });

  constructor(props: Props) {
    super(props);
    this.state = {
      fingerPosition: new Animated.ValueXY(),
      valuePosition: new Animated.ValueXY()
    };
    this.state.fingerPosition.addListener(({ x, y }) => {
      const dx = Math.max(Math.min(x, CIRCLE_RADIUS), -CIRCLE_RADIUS);
      const dy = Math.max(Math.min(y, CIRCLE_RADIUS), -CIRCLE_RADIUS);
      this.props.listener({
        absolute: [dx, dy],
        relative: [dx / CIRCLE_RADIUS, dy / CIRCLE_RADIUS]
      });
      Animated.spring(this.state.valuePosition, {
        toValue: { x: dx, y: dy }
      }).start();
    });

    this.panResponder = PanResponder.create({
      //Step 2
      onStartShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([
        null,
        {
          //Step 3
          dx: this.state.fingerPosition.x,
          dy: this.state.fingerPosition.y
        }
      ]),
      onPanResponderRelease: (e, gesture) => {
        Animated.spring(
          //Step 1
          this.state.fingerPosition, //Step 2
          { toValue: { x: 0, y: 0 } } //Step 3
        ).start();
      }
    });
  }

  render(): React.ReactNode {
    return (
      <View style={this.style.limits}>
        <Animated.View
          style={[this.state.valuePosition.getLayout(), this.style.value]}
        />
        <Animated.View
          {...this.panResponder.panHandlers}
          style={[this.state.fingerPosition.getLayout(), this.style.finger]}
        />
      </View>
    );
  }
}
