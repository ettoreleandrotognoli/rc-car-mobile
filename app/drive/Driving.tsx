import React from "react";
import Joystick, { JoystickValue } from "../gamepad/Joystick";
import { View, Text } from "react-native";
import { VehicleState } from "../vehicle";
import { Subject } from "rxjs";
import { map, debounce, debounceTime } from "rxjs/operators";

interface Props {
  listener: (vehicle: VehicleState) => void;
}

interface State { }

export default class Driving extends React.Component<Props, State> {
  protected joystickValueSubject: Subject<JoystickValue> = new Subject();

  constructor(props: Props) {
    super(props);
    this.joystickValueSubject
      .pipe(
        map(
          value => ({
            speed: -value.relative[1],
            direction: value.relative[0]
          }),
          debounceTime(100)
        )
      )
      .subscribe(this.props.listener);
  }

  render() {
    return (
      <View style={{ flex:1,alignContent: "center", alignSelf: "center" }}>
        <Text></Text>
        <Joystick listener={it => this.joystickValueSubject.next(it)} />
      </View>
    );
  }
}