import {
  Observable,
  Observer,
  from,
  forkJoin,
  Subject,
  BehaviorSubject,
  of
} from "rxjs";
import BluetoothSerial, {
  withSubscription
} from "react-native-bluetooth-serial-next";
import { map, mergeMap } from "rxjs/operators";

export enum BTDeviceStatus {
  Paired = 0,
  Unpaired
}

export interface BTDevice {
  raw: BluetoothSerial.CommonDevice;
}

export interface ConnectedBTDevice extends BTDevice {
  connection: any;
}

const asBtDevice = (rawDevice: BluetoothSerial.CommonDevice) =>
  ({
    raw: rawDevice
  } as BTDevice);

export class BluetoothManager {
  protected devicesMap: { [k: string]: BTDevice } = {};

  protected devicesSubject: Subject<BTDevice[]> = new BehaviorSubject<
    BTDevice[]
  >([]);

  constructor() {
    from(BluetoothSerial.isEnabled())
      .pipe(
        mergeMap(on => (on ? of(on) : BluetoothSerial.requestEnable())),
        mergeMap(on => (on ? of(on) : BluetoothSerial.enable()))
      )
      .subscribe(_ => {
        this.listPairDevices();
        this.listUnpairDevices();
        this.searchDevices();
      });
  }

  listPairDevices() {
    from(BluetoothSerial.list())
      .pipe(map(it => it.map(asBtDevice)))
      .subscribe(it => this.putDevices(it));
  }

  listUnpairDevices() {
    const asUnpairedDevice = asBtDevice(BTDeviceStatus.Unpaired);
    from(BluetoothSerial.list())
      .pipe(map(it => it.map(asBtDevice)))
      .subscribe(it => this.putDevices(it));
  }

  searchDevices() {
    from(BluetoothSerial.discoverUnpairedDevices())
      .pipe(map(it => it.map(asBtDevice)))
      .subscribe(it => this.putDevices(it));
  }

  public devices(): Observable<BTDevice[]> {
    return this.devicesSubject;
  }

  protected fireDevices() {
    this.devicesSubject.next(Object.values(this.devicesMap));
  }

  protected putDevices(devices: BTDevice[]) {
    devices.forEach(it => (this.devicesMap[it.raw.id] = it));
    this.fireDevices();
  }

  findDevices(): Observable<BluetoothSerial.CommonDevice[]> {
    BluetoothSerial.discoverUnpairedDevices();
    return from(BluetoothSerial.list());
  }

  connect(btDevice: BTDevice): Observable<ConnectedBTDevice> {
    const deviceId = btDevice.raw.id;
    return from(BluetoothSerial.isConnected(deviceId)).pipe(
      mergeMap(on =>
        on ? of(btDevice.raw) : BluetoothSerial.connect(deviceId)
      ),
      map(_ => BluetoothSerial.device(deviceId)),
      map(connection => ({ ...btDevice, connection }))
    );
  }
}
