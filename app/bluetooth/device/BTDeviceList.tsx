import React from "react";
import { View, Text, ScrollView, Alert, ActivityIndicator } from "react-native";
import BluetoothSerial from "react-native-bluetooth-serial-next";
import { from } from "rxjs";
import { mergeMap } from "rxjs/operators";
import BTDeviceItem from "./BTDeviceItem";
import { BluetoothManager, BTDevice, ConnectedBTDevice } from "..";

interface Props {
  deviceSelected: (btDevice: ConnectedBTDevice) => {}
}

interface State {
  devices: BTDevice[];
  loading: boolean;
}

const manager = new BluetoothManager();

export default class BTDeviceList extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      devices: [],
      loading: false
    };
  }

  componentDidMount() {
    manager.devices().subscribe(it =>
      this.setState({
        devices: it
      })
    );
  }

  connectToDevice(device: BTDevice) {
    console.log("connecting...", device);
    this.setState({ loading: true });
    manager
      .connect(device)
      .subscribe(
        it => this.props.deviceSelected(it),
        error => {
          Alert.alert("error", error.message);
          this.setState({ loading: false });
        },
        () => this.setState({ loading: false })
      );
  }

  render(): React.ReactNode {
    return (
      <View>
        <ActivityIndicator animating={this.state.loading} size="large" />
        <ScrollView>
          {this.state.devices.map(device => (
            <BTDeviceItem
              click={btDevice => this.connectToDevice(btDevice)}
              key={device.raw.id}
              device={device}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}
