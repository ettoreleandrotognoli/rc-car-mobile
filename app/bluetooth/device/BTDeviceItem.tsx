import { Device } from "react-native-ble-plx";
import { View, Text, Alert } from "react-native";
import { Icon } from "react-native-elements";
import React from "react";
import { BTDevice, BTDeviceStatus } from "..";

interface Props {
  device: BTDevice;
  click: (btDevice: BTDevice) => void;
}

const BTDeviceItem = (props: Props) => {
  return (
    <View
      onTouchEnd={evt => props.click(props.device)}
      style={{ flexDirection: "row", alignContent: "center"} }
    >
      <Icon name="bluetooth" type="font-awesome" />
      <Text>
        {props.device.raw.id} {props.device.raw.name || "unknown"}
      </Text>
    </View>
  );
};

export default BTDeviceItem;
