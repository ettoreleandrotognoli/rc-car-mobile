import _ from "underscore";
import { Buffer } from "buffer";

export interface VehicleState {
  speed: number;
  direction: number;
}

export const asBytes = (vehicleState: VehicleState) => {
  const buffer = new ArrayBuffer(8);
  const dataView = new DataView(buffer);
  dataView.setFloat32(0, vehicleState.speed, true);
  dataView.setFloat32(4, vehicleState.direction, true);
  return Buffer.from(buffer).toString("base64");
};
