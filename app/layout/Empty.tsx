import React from "react";
import { View } from "react-native";

type Action = (...args: any[]) => {}

interface LayoutOptions {
    component: any,
    properties: any,
    actions: { [name: string]: Action }
}

const DEFAULT_OPTIONS: LayoutOptions = {
    component: View,
    properties: {},
    actions: {}
}

interface Props {
    navigation: any;
}

export default class Empty extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
        console.log(props);
    }

    render() {
        const layoutOptions = Object.assign({}, DEFAULT_OPTIONS, this.props.navigation.state.params) as LayoutOptions
        const InnerComponent = layoutOptions.component;
        const props = layoutOptions.properties;
        Object.entries(layoutOptions.actions)
            .forEach(([name, action]) => {
                props[name] = (...args: any[]) => action(this.props.navigation, ...args)
            })
        return (
            <View>
                <InnerComponent {...props} />
            </View>
        )
    }
}